<?php

if (!defined('BASE_PATH')) {
    die('Direct access not permitted');
}

if(empty($_SESSION)) session_start();

function get_logged_in_userid(){
    return $_SESSION['id'];
}

function set_logged_in_userid($id, $token, $fingerprint) {
    $_SESSION['id'] = $id;
    $_SESSION['token'] = $token;
    $_SESSION['fingerprint'] = $fingerprint;
}

function get_ip_address(){
    return $_SERVER['REMOTE_ADDR'];
}