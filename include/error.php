<?php
//Set display error to off as we have our own handler
ini_set('display_errors', 0);

function labforum_halt_handler() {
    $error = error_get_last();
    if(ERROR_MODE === 'html' && !empty($error)):
        http_response_code(500);
        include dirname(__FILE__) . '/../template/laf-error.php';
        die;
    endif;
}

set_error_handler('labforum_halt_handler', E_ALL);
register_shutdown_function('labforum_halt_handler');