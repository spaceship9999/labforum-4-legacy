<?php

function check_slash($url, $param = null){
    if(substr($url, -1) !== '/')
    {
        header("Location: ${url}/" . (!empty($param)? "?${param}" : ''));
        exit;
    }
}

$request_uri_arr = explode('?', $_SERVER['REQUEST_URI']);
check_slash($request_uri_arr[0], (!empty($request_uri_arr[1])) ? $request_uri_arr[1]: '');