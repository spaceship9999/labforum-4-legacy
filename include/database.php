<?php

if (!defined('BASE_PATH')) {
    die('Direct access not permitted');
}

global $connection;
$connection = new \Medoo\Medoo([
        'database_type' => 'mysql',
        'database_name' => DB_NAME,
        'server' => DB_HOST,
        'username' => DB_USERNAME,
        'password' => DB_PASSWORD,
]);