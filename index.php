<?php
include_once 'labconfig.php';
include_once ROOT_DIR . '/include/autoload.php';
include_once ROOT_DIR . '/include/redirect.php';
include_once ROOT_DIR . '/include/error.php';
include_once ROOT_DIR . '/include/database.php';
include_once ROOT_DIR . '/include/session.php';



Labforum\Router::parent('/thread/{thread_id}', [
    'ThreadController@threadExists',
    'ThreadController@isNotDraft',
    'ThreadPermissionController@checkRights'
], function() {
    Labforum\Router::route('/' , function (){
        $query_arr = Labforum\Request::get_query_var_array();
        $thread = new Labforum\Thread();
        $thread->setQueryingThreadId($query_arr['thread_id']);
        $thread->getThread();
    });

    Labforum\Router::get('/poll/', [
        'PollController@canVote',
        'PollController@acceptingVotes',
    ], function(){
        //Your endpoint

    });
});

Labforum\Router::parent('/board/{board_id}', [
]);

Labforum\Router::parent('/login', function(){
    Labforum\Router::post('/', function(){
        $credentials = new \Manager\Credentials();
        $password_creds = array(
            'password' => $_POST['password'],
        );
        $credentials->loginBy('password', $_POST['username'], $password_creds);
    });
    Labforum\Router::get('/roadblock', function (){
        $login_trials = new \Manager\LoginTrials();
    });
});




$uri_split = explode('?', $_SERVER['REQUEST_URI']);
Labforum\Router::execute($uri_split[0]);