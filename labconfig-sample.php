<?php


/**
 * Global variable on DB.
 *
 */
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'xxxxx');
define('DB_HOST', 'localhost');
define('DB_NAME', 'labforum');
define('PREFIX', 'laf');


defined('ROOT_DIR') || define('ROOT_DIR', dirname(__FILE__));
defined('DIR') || define('DIR', dirname(__DIR__));

/*  Check whether it is on root directory to decide it's base path
 *  !! Always check if you are getting the right thing.
 */
defined('APP_PATH') || define('APP_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
defined('BASE_PATH') || define('BASE_PATH', str_replace($_SERVER['DOCUMENT_ROOT'], '', APP_PATH));

include_once ROOT_DIR . '/vendor/autoload.php';