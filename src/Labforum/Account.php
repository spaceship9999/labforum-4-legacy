<?php

namespace Labforum;

use Manager\Credentials;
use Manager\LoginTrials;

class Account
{
    public static $connections;
    public $user_id;
    protected $points = 0;


    public function __construct()
    {
        global $connection;
        $this->user_id =  (get_logged_in_userid())? get_logged_in_userid() : 0;
        Account::$connections = $connection;
    }

    public function registerValidators($handle, $func) {

    }


    public static function getIdByUsername($username)
    {
        return Account::$connections->get(PREFIX . '_user', 'id', [
            'username[=]' => $username,
        ]);
    }

    public function getCurrentUserId()
    {
        return $this->user_id;
    }

    public function incrementPoints()
    {
        $this->points++;
    }

}