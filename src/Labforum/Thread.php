<?php

namespace Labforum;
class Thread{
    public static $connection;
    public static $thread_id = null;
    public static $time_format = 'Y-m-d H:i';

    public function __construct()
    {
        global $connection;
        self::$connection = $connection;
    }

    public function setQueryingThreadId($thread_id)
    {
        self::$thread_id = $thread_id;
    }

    protected function getThreadDetails()
    {
        return self::$connection->get(PREFIX . '_post', [
            '[>]' . PREFIX . '_user' => ['author' => 'id'],
            '[>]' . PREFIX . '_board' => ['board_id' => 'id']
        ], [
            'title', 'content', PREFIX .'_post.created_date', 'author',
            'board_id', 'board_name', 'username', 'likes', 'shares', 'replyable',
            'show_operation_history', 'is_marked_hidden', 'is_marked_deleted',
            'is_poll_thread', 'permission',
        ], [
            PREFIX . '_post.id' => self::$thread_id,
        ]);
    }

    protected function getThreadOperationHistory()
    {
        if(empty(self::$thread_id)) return array();
        return self::$connection->select(PREFIX . '_thread_operations', [
            '[>]' . PREFIX . '_user' => ['operator' => 'id'],
        ], [
            'operation', 'date', 'operator', PREFIX .'_user.username', 'comment',
        ], [
            'thread_id' => self::$thread_id,
        ]);
    }

    protected function getThreadPoll()
    {
        return self::$connection->get(PREFIX . '_poll',[
            'content'
        ], [
            'thread_id' => self::$thread_id,
        ])['content'];
    }


    public function getReadPermission()
    {
        return self::$connection->get(PREFIX . '_post', 'permission',
        [
            'id' => self::$thread_id,
        ]);
    }

    public function getThread()
    {
        if(!empty(self::$thread_id)){
            $thread_details = $this->getThreadDetails();
            $thread_arr = array(
                'id' => self::$thread_id,
                'title' => $thread_details['title'],
                'author' => $thread_details['author'],
                'username' => $thread_details['username'],
                'board_id' => $thread_details['board_id'],
                'board_name' => $thread_details['board_name'],
                'content' => ($thread_details['is_marked_hidden'] == '1') ? null: $thread_details['content'],
                'date' => date(self::$time_format, strtotime($thread_details['created_date'])),
                'replyable' => $thread_details['replyable'],
                'is_marked_hidden' => $thread_details['is_marked_hidden'],
                'is_marked_deleted' => $thread_details['is_marked_deleted'],
                'operation_history' => $this->getThreadOperationHistory(),
                'is_poll_thread' => $thread_details['is_poll_thread'],
                'permission' => $thread_details['permission'],
            );
            if($thread_details['is_poll_thread'] == '1'){
                $thread_arr['poll'] = json_decode($this->getThreadPoll(), true);
            }
            $output = new OutputTools();
            $output->setData($thread_arr);
            $output->outputData('json', 200);
        }
    }

}