<?php


namespace Manager;

use Labforum\Account;
use Labforum\Meta;
use Labforum\OutputTools;

/**
 * Class Credentials
 *
 * A class that controls and manages credentials. You can override the
 * default implementation by extending this class
 *
 * @package Manager
 *
 */
class Credentials
{
    public $connection;
    protected static $validators = array();
    protected static $error_msg = array(
        'incorrect_credentials' => 'Incorrect credentials.',
        'trial_number_maxed_out' => 'Trial numbers has maxed out.',
    );

    public function __construct()
    {
        global $connection;
        $this->connection =& $connection;
        Credentials::registerValidators('password', array($this, 'passwordValidator'));
    }

    public static function setErrorMessageArr($message_arr)
    {
        //Allow filters to customise login error messages
        Credentials::$error_msg = $message_arr;
    }

    public static function getErrorMessageArr($message_arr): array
    {
        return Credentials::$error_msg;
    }

    public static function registerValidators($handle, $func)
    {
        if (is_callable($func)) {
            Credentials::$validators[$handle] = $func;
        }
    }

    /**
     * loginBy() - using a specific validator to login
     * @param $handle
     * @param $username
     * @param $credential
     * @param false $remember
     * @return bool
     */
    public function loginBy($handle, $username, $credential, $remember = false)
    {
        //Bring credentials check into custom handler
        $login_trials = new LoginTrials();
        $account = new Account();
        $output = new OutputTools();
        $tried_userid = $account->getIdByUsername($username);
        $tried_userid = !empty($tried_userid) ? $tried_userid : null;

        $data = array();

        if($login_trials->getIncorrectAttempts() >= (float)$login_trials::$max_login_trials){
            $remaining_time =  $login_trials->getRoadBlockRemainingTime();

            //No matter they are correct or not, we count that as incorrect
            $login_trials->incrementIncorrectAttempts(get_ip_address(), $tried_userid);
            $data = array(
                'error_msg' => Credentials::$error_msg['trial_number_maxed_out'],
                'error' => 'trial_number_maxed_out',
                'remaining_roadblock_time' => $remaining_time,
            );
            $output->setData($data);
            $output->outputData();

            return false;
        }


        if(!empty(Credentials::$validators[$handle])){
            //If the function returning false, the validator fails.
            if (!call_user_func_array(Credentials::$validators[$handle], array($tried_userid, $credential)))
            {
                $login_trials->incrementIncorrectAttempts(get_ip_address(), $tried_userid);
                $remaining_trials = $login_trials::$max_login_trials - $login_trials->getIncorrectAttempts();

                //Organise output data
                $data = array(
                    'error_msg' => Credentials::$error_msg['incorrect_credentials'],
                    'error' => 'incorrect_credentials',
                    'remaining_trials' => ($remaining_trials > 0) ? $remaining_trials : 0,
                );

                //Add time countdown if remaining trial is less than zero
                if ($remaining_trials <= 0){
                    $data['remaining_roadblock_time'] = $login_trials->getRoadBlockRemainingTime();
                }

                $output->setData($data);
                $output->outputData();

                return false;
            }

            //The credential is correct, let's flush session and add session variables

            $this->setLoginToken($tried_userid, $remember);

            $data = array(
                'success' => true,
            );

            $output->setData($data);
            $output->outputData();
        }
        return true;
    }

    protected function setLoginToken($id, $remember)
    {
        $token = bin2hex(openssl_random_pseudo_bytes(10));
        $signature = bin2hex(openssl_random_pseudo_bytes(10));
        $fingerprint = password_hash($signature, PASSWORD_DEFAULT);

        if(!isset($_SESSION)) session_start();
        session_regenerate_id();
        set_logged_in_userid($id, $token, $fingerprint);

        $expire_time = strtotime('+ 1 week');

        if($remember === true)
        {
            /* Only set cookie with hashed values of signature.
             * We are only using the fingerprint to prove that it's the user itself
             */
            setcookie('laf_token', $token, $expire_time);
            setcookie('fingerprint', $fingerprint, $expire_time);
        }
        $this->saveToken($id, $token, $signature);
    }

    /**
     * Write Login Token to db
     */
    protected function saveToken($id, $token, $signature)
    {
        $data = array(
            'id' =>  $id,
            'ua' => $_SERVER['HTTP_USER_AGENT'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'expires' => strtotime('+1 week'),
            'signature' => $signature,
        );

        $meta = new Meta();
        $meta->storeMeta('session_token_' . $token, $data);
    }

    /**
     * Don't trust user input ! Let's verify the fingerprint before giving away
     * @param $token
     * @param $user_fingerprint
     * @return false
     */
    public function validateLoginToken($token, $user_fingerprint){
        $meta = new Meta();
        $data = $meta->getMeta('session_token_' . $token);

        if(empty($data)) return false;

        /*
         * The token has been tampered with. Either the IP,  UA or signature doesn't match,
         * or simply someone is guessing the credentials.
         */
        if($data['ua'] !== $_SERVER['HTTP_USER_AGENT'] ||
            $data['ip'] !== $_SERVER['REMOTE_ADDR'] ||
            time() > $data['expires']){
            $this->clearCookies();
            return false;
        }
        if(!password_verify($data['signature'], $user_fingerprint)){
            $this->clearCookies();
            return false;
        }

        return true;
    }

    public function clearCookies()
    {
        session_destroy();
        setcookie('laf_token', '', time() - 3600);
        setcookie('fingerprint', '', time() - 3600);
    }

    /**
     * Validate whether the password matches for a specific user
     *
     * */
    public function passwordValidator($userid, $passwords){
        $user_details = $this->connection->get(PREFIX . '_user', [
            'username', 'password'
        ], [
            'id[=]' =>  $userid,
        ]);

        if(empty($user_details) || !password_verify($passwords['password'], $user_details['password'])) {
            return false;
        }
        return true;
    }
}