<?php

namespace Controller;

use Labforum;

class ThreadController{
    public $query_var;
    public $connection;
    public function __construct()
    {
        global $connection;
        //Getting query var and connection automatically
        $this->query_var = Labforum\Request::get_query_var_array();
        $this->connection =& $connection;
    }

    public function threadExists(): bool
    {
        if(!$this->connection->count(PREFIX . '_post', '*', [
            'id[=]' => $this->query_var['thread_id'],
        ])){
            $output_tools = new Labforum\OutputTools();
            $output_tools->setErrorMessage('Thread not exists.');
            $output_tools->outputData('json', 404);
        }
        return true;
    }

    public function isNotDraft(): bool {
        $query_var = Labforum\Request::get_query_var_array();
        if($this->connection->count(PREFIX . '_post', '*', [
            'id[=]' => $query_var['thread_id'],
            'is_draft[!]' => 0,
        ])){
            //Handle draft
            $output_tools = new Labforum\OutputTools();
            $output_tools->setErrorMessage('Thread not visible.');
            $output_tools->outputData('json', 403);
        }
        return true;
    }
}