<?php

namespace Controller;

use Labforum\Request;
use Labforum\OutputTools;

class ThreadPermissionController
{
    protected $query_var;
    public $connection;
    public function __construct()
    {
        global $connection;
        $this->query_var = Request::get_query_var_array();
        $this->connection =& $connection;
    }

    public function checkRights(): bool
    {
        $post_id = $this->query_var['thread_id'];
        $id = isset($_SESSION['id']) ? $_SESSION['id'] : '';
        $user_details = $this->connection->get(PREFIX . '_user',
            ['points', 'role_id'], [
            'id[=]' => $id,
        ]);

        $points = (!empty($user_details['points'])) ? $user_details['points']: 0;

        if(empty($id) || empty($user_details)){
            $user_read_permission = 0;
        }
        else if($user_details['role_id'] == '0'){
            $user_read_permission = $this->connection->get(PREFIX . '_permission', 'permission', [
                'min_points[>=]' => $points,
            ])['permission'];
        }

        $post_min_read_permission = $this->connection->get(PREFIX . '_post', 'permission', [
            'id[=]' => $post_id,
        ]);


        if($user_read_permission < $post_min_read_permission){
            $output_tools = new OutputTools();
            $output_tools->setErrorMessage('You have no rights to view the thread.');
            $output_tools->outputData('json', 403);
        }
        return true;
    }

    public function checkRole($post_id): bool
    {
        $id = isset($_SESSION['id']) ? $_SESSION['id'] : '';

        $required_roles = json_decode($this->connection->get(PREFIX . '_post', 'required_roles', [
            'id[=]' => $post_id,
        ])['required_roles']);


        if(empty($id)) return false;
        $current_role = $this->connection->get(PREFIX . '_user', 'role_id', [
            'id[=]' => $id
        ])['role_id'];

        foreach($required_roles['roles'] as $role){
            //If it has zero role, that means everyone can visit the thread
            if(intval($role) === 0) return true;
            if(intval($role) === intval($current_role)) return true;
        }
        return false;
    }
}