-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 11, 2021 at 11:32 PM
-- Server version: 10.5.5-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `labforum`
--

-- --------------------------------------------------------

--
-- Table structure for table `laf_board`
--

CREATE TABLE `laf_board` (
  `id` int(11) NOT NULL,
  `board_name` varchar(255) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `board_introduction` varchar(255) DEFAULT NULL,
  `board_rules` varchar(255) DEFAULT NULL,
  `min_author_rights` int(11) NOT NULL DEFAULT 0,
  `is_archived` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laf_board`
--

INSERT INTO `laf_board` (`id`, `board_name`, `created_date`, `board_introduction`, `board_rules`, `min_author_rights`, `is_archived`) VALUES
(1, 'Labstry General', '2021-03-22 00:00:00', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `laf_permission`
--

CREATE TABLE `laf_permission` (
  `id` int(11) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `permission` int(11) DEFAULT NULL,
  `min_points` int(11) DEFAULT NULL,
  `max_points` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `laf_poll`
--

CREATE TABLE `laf_poll` (
  `thread_id` int(11) NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`content`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `laf_poll`
--

INSERT INTO `laf_poll` (`thread_id`, `content`) VALUES
(1, '{\"question\": \"How about a poll?\",\"options\":[{\"key\": \"Yes\", \"value\": 5},{\"key\": \"no\", \"value\": 10}], \"deadline\": \"2020-01-01 23:59\"}');

-- --------------------------------------------------------

--
-- Table structure for table `laf_post`
--

CREATE TABLE `laf_post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `board_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `likes` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `shares` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `is_draft` int(11) DEFAULT NULL,
  `is_marked_deleted` int(11) DEFAULT NULL,
  `is_marked_hidden` int(11) DEFAULT NULL,
  `replyable` int(11) DEFAULT NULL,
  `show_operation_history` int(11) NOT NULL DEFAULT 1,
  `is_poll_thread` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laf_post`
--

INSERT INTO `laf_post` (`id`, `title`, `content`, `created_date`, `board_id`, `author`, `views`, `likes`, `shares`, `is_draft`, `is_marked_deleted`, `is_marked_hidden`, `replyable`, `show_operation_history`, `is_poll_thread`) VALUES
(1, 'Test title', 'Test Content', '2021-03-08 00:00:00', 1, 1, 0, NULL, NULL, 0, 0, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `laf_reply`
--

CREATE TABLE `laf_reply` (
  `thread_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `reply_date` datetime NOT NULL DEFAULT current_timestamp(),
  `author` int(11) DEFAULT NULL,
  `is_marked_hidden` int(11) DEFAULT 0,
  `is_marked_deleted` int(11) DEFAULT 0,
  `is_draft` int(11) DEFAULT 0,
  `likes` int(11) NOT NULL DEFAULT 0,
  `shares` int(11) NOT NULL DEFAULT 0,
  `show_operation_history` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laf_reply`
--

INSERT INTO `laf_reply` (`thread_id`, `id`, `title`, `content`, `reply_date`, `author`, `is_marked_hidden`, `is_marked_deleted`, `is_draft`, `likes`, `shares`, `show_operation_history`) VALUES
(1, 1, 'Try', 'Try', '2021-03-22 20:24:42', NULL, 0, 0, 0, 0, 0, 1),
(1, 2, 'Try2', 'Try2', '2021-03-22 20:24:57', NULL, 0, 0, 0, 0, 0, 1),
(2, 3, 'Try', 'Try', '2021-03-22 20:25:09', NULL, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `laf_thread_operations`
--

CREATE TABLE `laf_thread_operations` (
  `thread_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `operation` text NOT NULL,
  `operator` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laf_thread_operations`
--

INSERT INTO `laf_thread_operations` (`thread_id`, `date`, `operation`, `operator`, `comment`) VALUES
(1, '2021-03-22 20:29:50', 'moved', 1, 'Moved to Board 1 due to off-topic');

-- --------------------------------------------------------

--
-- Table structure for table `laf_user`
--

CREATE TABLE `laf_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `register_date` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT 0,
  `role_id` int(11) NOT NULL DEFAULT 0,
  `email` text NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `laf_user`
--

INSERT INTO `laf_user` (`id`, `username`, `register_date`, `points`, `role_id`, `email`, `password`) VALUES
(1, 'littlephone', '2021-03-08 22:16:46', 0, 0, 'raynold_chow@hotmail.com', '$2y$10$0N5ft2P6W8Hy8zqcx8aPCemc6svLh/q/WKGwHdJ0Ly68d6wtvkjHa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `laf_board`
--
ALTER TABLE `laf_board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laf_permission`
--
ALTER TABLE `laf_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laf_poll`
--
ALTER TABLE `laf_poll`
  ADD PRIMARY KEY (`thread_id`);

--
-- Indexes for table `laf_post`
--
ALTER TABLE `laf_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laf_reply`
--
ALTER TABLE `laf_reply`
  ADD PRIMARY KEY (`id`,`thread_id`);

--
-- Indexes for table `laf_thread_operations`
--
ALTER TABLE `laf_thread_operations`
  ADD PRIMARY KEY (`thread_id`,`date`);

--
-- Indexes for table `laf_user`
--
ALTER TABLE `laf_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `laf_board`
--
ALTER TABLE `laf_board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `laf_permission`
--
ALTER TABLE `laf_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laf_post`
--
ALTER TABLE `laf_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `laf_reply`
--
ALTER TABLE `laf_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `laf_user`
--
ALTER TABLE `laf_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
